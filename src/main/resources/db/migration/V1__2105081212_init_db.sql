CREATE TABLE `user` (
	id BIGINT auto_increment NOT NULL,
	email varchar(100) NOT NULL,
	password varchar(250) NOT NULL,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	`role` varchar(100) NOT NULL,
	created_at DATETIME DEFAULT NOW() NOT NULL,
	updated_at DATETIME NULL,
	created_by BIGINT NULL,
	updated_by BIGINT NULL,
	CONSTRAINT user_PK PRIMARY KEY (id)
);

CREATE TABLE category (
	id BIGINT auto_increment NOT NULL,
	name varchar(1000) NOT NULL,
	slug varchar(1000) NOT NULL,
	created_at DATETIME DEFAULT NOW() NOT NULL,
	updated_at DATETIME NULL,
	created_by BIGINT NULL,
	updated_by BIGINT NULL,
	CONSTRAINT category_PK PRIMARY KEY (id)
);

CREATE TABLE article (
	id BIGINT auto_increment NOT NULL,
	title varchar(1000) NOT NULL,
	slug varchar(2000) NOT NULL,
	description varchar(2000) NOT NULL,
	content text NOT NULL,
	thumbnail varchar(1000) NULL,
	category_id BIGINT NOT NULL,
	created_at DATETIME DEFAULT NOW() NOT NULL,
	updated_at DATETIME NULL,
	created_by BIGINT NULL,
	updated_by BIGINT NULL,
	CONSTRAINT category_FK FOREIGN KEY (category_id) REFERENCES category(id),
	CONSTRAINT article_PK PRIMARY KEY (id)
);