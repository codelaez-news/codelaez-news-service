package com.codelaez.news.common.exception;

import com.codelaez.news.common.exception.error.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CodelaezNewsException extends RuntimeException {
    private final ErrorCode code;
}
