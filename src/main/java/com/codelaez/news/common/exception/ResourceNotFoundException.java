package com.codelaez.news.common.exception;

import com.codelaez.news.common.exception.error.ErrorCode;

public class ResourceNotFoundException extends CodelaezNewsException {

    public ResourceNotFoundException() {
        super(ErrorCode.RESOURCE_NOT_FOUND);
    }
}
