package com.codelaez.news.common.exception.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    SYSTEM_ERROR("001", "An internal server error occured"),
    RESOURCE_NOT_FOUND("002", "Resource not found"),
    REQUEST_IS_INVALID("003", "Request is invalid");

    private final String code;
    private final String message;
}
