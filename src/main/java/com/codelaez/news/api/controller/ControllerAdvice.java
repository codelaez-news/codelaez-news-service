package com.codelaez.news.api.controller;

import com.codelaez.news.common.exception.CodelaezNewsException;
import com.codelaez.news.common.exception.ResourceNotFoundException;
import com.codelaez.news.common.exception.error.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handle(ResourceNotFoundException ex) {
        return ErrorResponse.builder()
            .code(ex.getCode().getCode())
            .message(ex.getCode().getMessage())
            .build();
    }

    @ExceptionHandler(value = CodelaezNewsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handle(CodelaezNewsException ex) {
        return ErrorResponse.builder()
            .code(ex.getCode().getCode())
            .message(ex.getCode().getMessage())
            .build();
    }
}
